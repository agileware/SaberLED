#pragma once

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class Elapsed {
	private:
		unsigned long ms;
	public:
		Elapsed(void) 					{ ms = millis(); }
		Elapsed(unsigned long val) 		{ ms = millis() - val; }
		Elapsed(const Elapsed &orig) 	{ ms = orig.ms; }

		operator unsigned long () const 				{ return millis() - ms; }
		Elapsed& operator = (const Elapsed &rhs) 		{ ms = rhs.ms; return *this; }
		Elapsed& operator = (unsigned long val) 		{ ms = millis() - val; return *this; }
		Elapsed& operator -= (unsigned long val)      	{ ms += val ; return *this; }
		Elapsed& operator += (unsigned long val)      	{ ms -= val ; return *this; }
		Elapsed  operator - (int val) const           	{ Elapsed r(*this); r.ms += val; return r; }
		Elapsed  operator - (unsigned int val) const  	{ Elapsed r(*this); r.ms += val; return r; }
		Elapsed  operator - (long val) const          	{ Elapsed r(*this); r.ms += val; return r; }
		Elapsed  operator - (unsigned long val) const 	{ Elapsed r(*this); r.ms += val; return r; }
		Elapsed  operator + (int val) const           	{ Elapsed r(*this); r.ms -= val; return r; }
		Elapsed  operator + (unsigned int val) const  	{ Elapsed r(*this); r.ms -= val; return r; }
		Elapsed  operator + (long val) const          	{ Elapsed r(*this); r.ms -= val; return r; }
		Elapsed  operator + (unsigned long val) const 	{ Elapsed r(*this); r.ms -= val; return r; }
};
