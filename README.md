[![CC BY-NC-SA](https://licensebuttons.net/l/by-nc-sa/3.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

# Build dependencies

 * [MicroDebug](https://github.com/rlogiacco/MicroDebug) 1.1.0
 * [CircularBuffer](https://github.com/rlogiacco/CircularBuffer) 1.3.1 
  
## Build Notes

This firmware has been developed for the [ArteKit PropBoard](https://www.artekit.eu/doc/guides/propboard-manual/), a board specifically built for these type of applications. It is compatible with the Arduino IDE, but we use Sloeber for our development: in both cases you need to add the PropBoard toolchain distributed by ArteKit for free via the URL `https://www.artekit.eu/software/package_artekit_index.json`.
Don't forget to switch your build environment selecting the PropBoard build configuration.

# Operation Instructions

The SaberLED uses one single button placed on the hilt as control and provides the following interactions:

 * single short click ignites/retracts the blade
 * long click
   * on blade retracted, plays theme anthem while  kept pressed, then activates ignition on release
   * activates stab mode on blade ignited
 * double short click
   * change blade color on blade retracted
   * switch between parry and attack mode on blade ignited
 * triple short click on blade retracted switches side (imperial/rebels)

# Schematics and circuit

**WARNING** in our latest revision we have completely avoided the creation of a PCB, these information are left here for historical reasons only.

Both schematics and printed circuit board (PCB) are available in this repository in a non editable format:

 * `/schematics.pdf` is a multi page PDF where the whole circuit is split into multiple logical sections for ease of reference
 * the `/pcb` folder contains multiple version of the board in Gerber format
  * `/pcb/rev. 0` contains the Gerbers for the first prototype board, mostly using through hole components
  * `/pcb/rev. A` contains the first iteration of the circuit board in Gerber format

The editable version for these files is available through [CircuitMaker](https://workspace.circuitmaker.com/) public repository, each of the above versions in a different project:

 * the through hole version is contained in the [SaberLED](https://workspace.circuitmaker.com/Projects/Details/Roberto-Lo-Giacco/SaberLED) project
 * the compact, SMD version, is within a project called [SaberLED SMD](https://workspace.circuitmaker.com/Projects/Details/Roberto-Lo-Giacco/SaberLED-SMD)

You can contribute to those projects either by joining the the project on CircuitMaker or by forking the projects.

# 3D models
 
The plastic inserts and the whole assembly has been modeled using [Autodesk Fusion 360](https://www.autodesk.com/products/fusion-360/students-teachers-educators).
All the plastic inserts have been published in a non editable **STL** format in this repository under the `/models` folder:

 * the `/models/35mm` folder contains files specific to the 35mm diameter handle/hilt
 * the files contained in the `/models/40mm` are specific to the 40mm diameter handle/hilt
 * the files contained in the `/models` folder are valid for both diameters

We modeled three different versions of the end cap: `honeycomb`, `voronoi` and `concentric`, but you only need one of those, pick your favorite.

The same models are also available in an editable format via the Autodesk cloud platform A360: just ask the project members/collaborators to get direct access to the project or use the following links:
 * shared
   * [spacers](http://a360.co/2EwCFCf)
   * [blade](http://a360.co/2BPOov1)
 * caps
   * [concentric](http://a360.co/2BgwKPO)
   * [honeycomb](http://a360.co/2sca7JC)
   * [voronoi](http://a360.co/2EqQ9zz)
 * 35mm
   * [electronics holder](http://a360.co/2G3zVJQ)
   * [sloped bars](http://a360.co/2G1q47u)
   * [deco band](http://a360.co/2G1q47u)

# Sound files

The sound files are stored on the SD/microSD card and must all be in the same format: WAV 16-bit, 22050fs (mono and stereo can be mixed up).

By default the firmware supports the following files:

 * `boot.wav` played when the saber has succesfully finished the boostrap sequence
 * `on.wav` and `off.wav` are played when saber is turned on and off respectively
 * `hum.wav` is played in the background when the saber is turned on, in a continous loop
 * `swing0.wav`, `swing1.wav`, `swing2.wav`, `swing3.wav`, `swing4.wav`, `swing5.wav`, `swing6.wav` and `swing7.wav` are randomly selected when saber is swinged in any direction
 * `hit0.wav`, `hit1.wav`, `hit2.wav` and `hit3.wav` are randomly picked when an hit is detected
 * `blaster0.wav`, `blaster1.wav`, `blaster2.wav`, `blaster3.wav` and `blaster4.wav` are randomly picked when in parry mode and a swing is detected
 * `shutdown.wav` and `battery-low.wav` are used to notify battery status, with the former used when battery reaches critical condition

If you ever decide to change the `hum.wav` file, take into consideration the audio must loop nicely!