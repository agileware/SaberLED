#pragma GCC diagnostic ignored "-Wwrite-strings"
#pragma once

#define SERIAL_DEBUG false
#include <SerialDebug.h>

#define BUTTON_PIN 12
// battery parameters
#define BATT_MIN_VOLTAGE    3000
#define BATT_CHECK_FREQ    60000
#define BATT_WARNING_FREQ 180000

// random sounds
#define SWING_COUNT 	8
#define HIT_COUNT 		3
#define DEFLECT_COUNT 	5
#define THEME_IMPERIAL	0
#define THEME_REBELS 	1
#define AUDIO_VOLUME	8
uint8_t theme;
char* themes[] = {"imperial.wav", "rebels.wav"};
WavPlayer bkgSound, fxSound, batSound;

// blade parameters
//#define CRYSTAL_ENABLED
#define CRYSTAL_EN_PIN		4
#define CRYSTAL_SENSE_PIN	5
#define NUM_LEDS 			236
#define HIT_HUE_VARIATION 	20
#define HIT_HUE_LATENCY 	5
#define DEFLECT_HUE_VARIATION 60
#define DEFLECT_LATENCY 	10
#define SABER_SPEED 		128
#define SABER_BRIGHTNESS 	232 //218
#define BITS_PER_PIXEL		4
#define NUM_COLORS			(1<<BITS_PER_PIXEL)
#define COLOR_OFF 		0
#define COLOR_ON  		2
#define COLOR_HIT_LO 	4
#define COLOR_HIT_HI 	9
#define COLOR_BLASTER 	10

#include "RGBColor.h"
#include <LedStrip.h>
LedStrip strip;
COLOR palette[NUM_COLORS];

#include "Elapsed.h"
Elapsed clicked;
uint8_t clicks = 0;
#define BUTTON_CLICK_DURATION 400
#define BUTTON_LONG_DURATION 500

#define OFF				0
#define RETRACT			1
#define IGNITE			2
#define ON				3
#define SWING			4
#define HIT				5
#define STAB			6
#define PARRY			7
#define DEFLECT			8
#define PLAY			9
#define BATTERY_PROTECT	10
uint8_t status = OFF;


#define NUM_HUES 4
uint16_t hues[NUM_HUES] = {0,120,240,300};
uint8_t hueIndex = 0;

#include <PropButton.h>
PropButton button;

void setup() {
	SERIAL_DEBUG_SETUP(9600);
	delay(100);

	DEBUG("BOOTING UP...");
	#ifdef CRYSTAL_ENABLED
		setupStrip();
	#else
		setupStrip(hues[hueIndex]);
	#endif
	setupAudio();
	setupAccel();
	setupButton();

	DEBUG("BOOT COMPLETED!");
}

void loop() {
	checkButton();
	checkBattery();
	if (status != OFF || status != BATTERY_PROTECT || status != STAB || status != PLAY) {
		switch (status) {
			case HIT:
				hit();
				break;
			case SWING:
				swing();
				break;
			case IGNITE:
				on();
				break;
			case RETRACT:
				off();
				break;
			case DEFLECT:
				deflect();
				break;
		}
	} else if (status == BATTERY_PROTECT && !fxSound.playing()) {
		enterLowPowerMode(1, FALLING);
	}
}

/******************
 *     EVENTS     *
 ******************/

void inline __attribute__ ((always_inline)) on() {
	DEBUG("IGNITION");
	Elapsed duration = 0;
	fxSound.play("on.wav");
	uint latency = (float) (fxSound.duration() * 100) / (NUM_LEDS / 2);
	for (uint16_t i = 0; i < NUM_LEDS / 2; i++) {
		strip.set(i + 1, palette[COLOR_ON]);
		strip.set(NUM_LEDS - i, palette[COLOR_ON]);
		strip.update();
		delayMicroseconds(latency);
	}
	strip.update();
	bkgSound.play("idle.wav", PlayModeLoop);
	status = ON;
	DEBUG("SABER ON", fxSound.duration(), duration);
}

void inline __attribute__ ((always_inline)) off() {
	DEBUG("RETRACTION");
	Elapsed duration = 0;
	bkgSound.stop();
	fxSound.play("off.wav");
	uint latency = (float) (fxSound.duration() * 100) / (NUM_LEDS / 2);
	for (uint16_t i = 0; i < NUM_LEDS / 2; i++) {
		strip.set(NUM_LEDS / 2 + i + 1, palette[COLOR_OFF]);
		strip.set(NUM_LEDS / 2 - i, palette[COLOR_OFF]);
		strip.update();
		delayMicroseconds(latency);
	}
	strip.update();
	status = OFF;
	DEBUG("SABER OFF", fxSound.duration(), duration);
}

void inline __attribute__ ((always_inline)) swing() {
	DEBUG("SWING...");
	fxSound.playRandom("swing", 0, SWING_COUNT - 1);
	status = ON;
}

void inline __attribute__ ((always_inline)) hit() {
	DEBUG("HIT !!!!");
	fxSound.playRandom("hit", 0, HIT_COUNT - 1);
	for (uint8_t i = 0; i < 3; i++) {
		blade(COLOR_HIT_LO + i);
		delay(HIT_HUE_LATENCY);
		blade(COLOR_HIT_HI - i);
		delay(HIT_HUE_LATENCY);
	}
	blade(COLOR_ON);
	status = ON;
}

void inline __attribute__ ((always_inline)) deflect() {
	DEBUG("DEFLECT!");
	fxSound.playRandom("blaster", 0, DEFLECT_COUNT - 1);
	uint16_t length = NUM_LEDS / 10; // 23
	uint8_t section = random(4) * NUM_LEDS / 2 + (length / 2);
	DEBUG("section", section + 1, section + length, NUM_LEDS - section - length, NUM_LEDS - section);
	for (uint8_t i = 0; i < 3; i++) {
		strip.setRange(section + 1, section + length, palette[COLOR_BLASTER + i]);
		strip.setRange(NUM_LEDS - section - length, NUM_LEDS - section, palette[COLOR_BLASTER + i]);
		strip.update();
		delay(DEFLECT_LATENCY);
	}
	for (uint8_t i = 2; i <= 0; i--) {
		strip.setRange(section + 1, section + length, palette[COLOR_BLASTER + i]);
		strip.setRange(NUM_LEDS - section - length, NUM_LEDS - section, palette[COLOR_BLASTER + i]);
		strip.update();
		delay(DEFLECT_LATENCY);
	}
	blade(COLOR_ON);
	status = PARRY;
}

/******************
 *   FUNCTIONS    *
 ******************/

void inline __attribute__ ((always_inline)) blade(uint8_t color) {
	strip.set(0, palette[color]);
	strip.update();
}

#include <Battery.h>
#include <CircularBuffer.h>
Elapsed batteryCheck, warning;
CircularBuffer<uint16_t, 10> queue;

void inline __attribute__ ((always_inline)) checkBattery() {
	if (batteryCheck > BATT_CHECK_FREQ) {
		batteryCheck = 0;
		queue.push(readBattery());
		uint16_t voltage = 0;
		for (uint8_t i = 0; i < queue.size(); i++) {
			voltage += queue[i];
		}
		voltage /= queue.size();
		uint8_t level = sigmoidal(voltage, BATT_MIN_VOLTAGE, 4200);
		DEBUG("battery voltage", voltage, level);
		if (status == OFF || status == BATTERY_PROTECT || queue.size() < 3) {
			return;
		}
		if (level < 10 && level >= 2) {
			if (warning > BATT_WARNING_FREQ) {
				// play audio every 3 minutes
				batSound.play("battery-low.wav");
				warning = 0;
			}
			for (uint8_t i = 0; i < 3 && status != OFF; i++) {
				blade(COLOR_OFF);
				delay(100);
				blade(COLOR_ON);
				delay(100);
			}
		} else if (level < 2) {
			// battery is dying: let's protect it from unrecoverable discharging
			off();
			status = BATTERY_PROTECT;
			batSound.play("shutdown.wav");
		}
	}
}

void inline __attribute__ ((always_inline)) checkButton() {
	switch (button.getEvent()) {
		case ButtonShortPressAndRelease:
			DEBUG("button short press");
			clicks++;
			clicked = 0;
			break;
		case EventNone:
			if (clicks > 0 && clicked > BUTTON_CLICK_DURATION) {
				DEBUG("button ...", clicks);
				if (status == BATTERY_PROTECT) {
					strip.ramp(palette[COLOR_ON], palette[COLOR_OFF], 200);
					clicks = 0;
					break;
				}
				switch (clicks) {
					case 0:
						break;
					case 1:
						DEBUG("button click");
						if (status == OFF) {
							status = IGNITE;
						} else if (status == ON || status == PARRY || status == STAB) {
							status = RETRACT;
						}
						break;
					case 2:
						DEBUG("button double click");
						if (status == OFF) {
							status = IGNITE;
							setupStrip(hues[++hueIndex % NUM_HUES]);
						} else if (status == ON) {
							status = PARRY;
						} else if (status == PARRY) {
							status = ON;
						}
						break;
					default:
						DEBUG("button triple click");
						if (status == OFF) {
							theme = ++theme % 2;
							status = IGNITE;
							setupStrip(hues[theme]);
						}
				}
				clicks = 0;
			}
			break;
		case ButtonLongPressed:
			DEBUG("button long press");
			if (status == OFF) {
				status = PLAY;
				DEBUG("theme", theme, themes[theme]);
				fxSound.play(themes[theme], PlayModeNormal);
			} else if (status == ON) {
				status = STAB;
				// to be implemented
			}
			button.resetEvents();
	        break;
		case ButtonReleased:
			DEBUG("button released");
			if (status == STAB) {
				// return to IDLE
				status = ON;
				// blade(COLOR_ON);
				// bkgSound.play("idle.wav", PlayModeLoop);
			} else if (status == PLAY) {
				status = IGNITE;
			}
			break;
		default:
			break;
	}
}

/******************
 *   INTERRUPTS   *
 ******************/

void swingDetected() {
	if (status == ON) {
		status = SWING;
	} else if (status == PARRY) {
		status = DEFLECT;
	}
}

void hitDetected() {
	if (status == ON || status == SWING) {
		status = HIT;
	} else if (status == PARRY) {
		status = DEFLECT;
	}
}

/******************
 * SETUP ROUTINES *
 ******************/
void setupAccel() {
	if (Motion.begin()) {
		// swing detection
		float pulseIntensity = 3.78f; // 6, 4.5f, 4, 2.5f, 1
		int pulseDuration = 100; // 500, 100, 50, 40, 20
		int pulseLatency = 250; // 200, 150, 100, 50, 25
		Motion.configPulse(AxisAll, pulseIntensity, pulseDuration, pulseLatency, MotionInterrupt1);
		Motion.attachInterrupt(MotionInterrupt2, swingDetected);

		// hit detection
		float transientIntensity = 0.6f; // 0.9f, 0.7f, 0.6f, 0.5f, 0.3f
		int transientDuration = 50; // 80, 70, 50, 20, 20
		Motion.configTransient(AxisAll, transientIntensity, transientDuration, MotionInterrupt2);
		Motion.attachInterrupt(MotionInterrupt1, hitDetected);

		DEBUG("  MOTION OK");
	}
}

void setupAudio() {
	if (Audio.begin(22050, 16, true)) {
		Audio.setVolume(AUDIO_VOLUME);
		Audio.unmute();
		fxSound.play("boot.wav", PlayModeNormal);

		DEBUG("  AUDIO OK", AUDIO_VOLUME);
	}
}
void setupStrip(uint16_t hue) {
	strip.begin(NUM_LEDS);
	strip.setRange(1, NUM_LEDS, 0, 0, 0);
	strip.update(0, true);

#ifdef CRYSTAL_ENABLED
	// determine hue
	pinMode(CRYSTAL_SENSE_PIN, INPUT_ANALOG);
	pinMode(CRYSTAL_EN_PIN, OUTPUT);
	digitalWrite(CRYSTAL_EN_PIN, HIGH);
	hue = map(analogRead(CRYSTAL_SENSE_PIN), 0, 1023, 0, 359);
	digitalWrite(CRYSTAL_EN_PIN, LOW);
#endif
	DEBUG("  CRYSTAL OK", hue);

	// set strip palette
	RGBColor color;
	color.fromHSV(hue, 0, 0);
	palette[0] = RGB(color.red, color.green, color.blue);
	color.fromHSV(hue, 255, SABER_BRIGHTNESS / 2);
	palette[1] = RGB(color.red, color.green, color.blue);
	color.fromHSV(hue, 255, SABER_BRIGHTNESS);
	palette[2] = RGB(color.red, color.green, color.blue);

	color.fromHSV(hue - (HIT_HUE_VARIATION / 4), 255, SABER_BRIGHTNESS);
	palette[4] = RGB(color.red, color.green, color.blue);
	color.fromHSV(hue - (HIT_HUE_VARIATION / 2), 255, SABER_BRIGHTNESS);
	palette[5] = RGB(color.red, color.green, color.blue);
	color.fromHSV(hue - (HIT_HUE_VARIATION), 255, SABER_BRIGHTNESS);
	palette[6] = RGB(color.red, color.green, color.blue);
	color.fromHSV(hue + (HIT_HUE_VARIATION), 255, SABER_BRIGHTNESS);
	palette[7] = RGB(color.red, color.green, color.blue);
	color.fromHSV(hue + (HIT_HUE_VARIATION / 2), 255, SABER_BRIGHTNESS);
	palette[8] = RGB(color.red, color.green, color.blue);

	color.fromHSV(hue + (DEFLECT_HUE_VARIATION / 3), 255, SABER_BRIGHTNESS);
	palette[10] = RGB(color.red, color.green, color.blue);
	color.fromHSV(hue + (DEFLECT_HUE_VARIATION / 2), 255, SABER_BRIGHTNESS);
	palette[11] = RGB(color.red, color.green, color.blue);
	color.fromHSV(hue + DEFLECT_HUE_VARIATION, 255, SABER_BRIGHTNESS);
	palette[12] = RGB(color.red, color.green, color.blue);

	theme = palette[2].r > 16 ? THEME_IMPERIAL : THEME_REBELS;
	DEBUG("  THEME", themes[theme]);
	DEBUG("  PALETTE OK", palette[2].r, palette[2].g, palette[2].b);
	DEBUG("  LED STRIP OK", NUM_LEDS);
}

void setupButton() {
	button.begin(BUTTON_PIN, ButtonActiveLow);
	button.setLongPressTime(BUTTON_LONG_DURATION);
	DEBUG("  BUTTON OK", BUTTON_PIN);
}
